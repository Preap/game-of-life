function make2DArray(cols,rows){
  let array = new Array(cols);
  for(let i = 0; i < cols; i++){
    array[i] = new Array(rows);
    for(let j = 0; j < rows; j++){
      array[i][j] = 0;
    }
  }
  return array;
}

let pauseLife = 0;
let heigh = 1600;
let widht = 900;
let cols =  50;
let rows = 50;
let space = heigh/cols;
let fps=60;
let deadColor;
let aliveColor;
let grid = make2DArray(cols,rows);
let tempGrid = make2DArray(cols,rows);
let nextGrid = make2DArray(cols,rows);
function setup() {
  
  createCanvas(heigh,widht);
  background(0);
  frameRate(fps);
  randomizeGrid(grid);
  deadColor = [random(127),random(127),random(127)];
  aliveColor = [deadColor[0]*2,deadColor[1]*2,deadColor[2]*2];
}

function computeNext(grid,x,y){
  let sum = 0;
  /* Using modulo cols and rows - 1 leads to a wraparound world */
  sum += grid[(x-1+cols) % cols][(y-1+rows) % rows];
  sum += grid[x % cols][(y-1+rows) % rows];
  sum += grid[(x+1) % cols][(y-1+rows) % rows];
  
  sum += grid[(x-1+cols) % cols][y];
  sum += grid[(x+1) % cols][y];

  sum += grid[(x-1+cols) % cols][(y+1) % rows];
  sum += grid[x][(y+1) % rows];
  sum += grid[(x+1) % cols][(y+1) % rows];

  /* Testing the rules */
  if(grid[x][y] == 0 && sum == 3){
    return 1;
  } else if(grid[x][y] == 1 && (sum < 2 || sum > 3)){
    return 0;
  } else {
    return grid[x][y];
  }
}

function draw(){
  if(!pauseLife)
  { 
    tempGrid = grid;
    createGrid(grid);
    for(i=0;i<cols;i++)
      for(j=0;j<rows;j++)
      {
        nextGrid[i][j] = computeNext(grid,i,j);
      }
    grid = nextGrid;
  }
}

function mousePressed()
{
  pauseLife = !pauseLife;
}


function createGrid(theGrid)
{
  for(i=0;i<cols;i++)
    for(j=0;j<rows;j++)
    {
      if(theGrid[i][j])
      {
        ellipse(i*space,j*space,space,space);
        fill(aliveColor);
        stroke(aliveColor);
      }
      else{
        ellipse(i*space,j*space,space,space);
        fill(deadColor);
        stroke(deadColor);
      }
    }
}

function randomizeGrid(theGrid)
{
  for(i=0;i<cols;i++)
    for(j=0;j<rows;j++)
    {
      let num = floor(random(3));
      if(num == 0)
      {
        theGrid[i][j]=0;
      }
      else
      {
        theGrid[i][j]=1;
      }
    } 
}